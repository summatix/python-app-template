# Python App Template

This is a base template for creating a python pip application.

### Install

### OSX

To use this, you'll need to install cookiecutter:

    $ brew install cookiecutter

### Linux

If you are not on a Mac, you may instlal cookiecutter via pip.

    $ pip install cookiecutter

## Usage

To generate a project with the template, you simply need to run the following:


    $ cookiecutter ssh://git@bitbucket.org/summatix/python-app-template.git

Follow the prompts and you'll have a fresh customized copy of a python app.

## Variables

Templates allow you define variables that will get replaced in specific
locations within the template. Places where you see
'{{ cookiecutter.<variable_name>}}' are variables defined in the
`cookiecutter.json` file. The list of variables for this template are:

### General variables

* `repo_name` - The name of the python app. Usually a one word noun such as
"skeleton" or "search." Can only contain lowercase characters (a-z). Multiple
words concatenated without spaces/dashes/underscores/dots are acceptable.
* `project_title` - Name of the project usually used for places where a title
is needed.
* `version` - The version of the app.

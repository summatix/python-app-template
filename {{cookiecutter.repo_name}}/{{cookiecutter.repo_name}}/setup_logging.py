import logging

import colorlog


logger = logging.getLogger("{{cookiecutter.repo_name}}")


def setup(debug=False):
    """Sets up colored logging to the console.

    Args:
        debug (bool): Whether or not to enable debugging. Defaults to False.
    """
    handler = logging.StreamHandler()
    handler.setLevel(logging.DEBUG)
    handler.setFormatter(colorlog.ColoredFormatter(
        "%(log_color)s%(message)s",
        log_colors={
            "DEBUG":    "cyan",
            "INFO":     "green",
            "WARNING":  "yellow",
            "ERROR":    "red",
            "CRITICAL": "red,bg_white",
        }
    ))

    if debug:
        logger.setLevel(logging.DEBUG)

    else:
        logger.setLevel(logging.INFO)

    logger.addHandler(handler)

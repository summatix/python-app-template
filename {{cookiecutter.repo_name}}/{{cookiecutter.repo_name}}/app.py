import logging

import click

import {{cookiecutter.repo_name}}.setup_logging


logger = logging.getLogger("{{cookiecutter.repo_name}}")


@click.command()
@click.option("--verbose", is_flag=True, default=False)
def {{cookiecutter.repo_name}}_cmd(verbose):
    {{cookiecutter.repo_name}}.setup_logging.setup(verbose)
    logger.debug("Starting application")
    logger.info("{{cookiecutter.project_title}} welcomes you")


def cli():
    """Entry point to the application.
    """
    {{cookiecutter.repo_name}}_cmd()


if __name__ == "__main__":
    cli()

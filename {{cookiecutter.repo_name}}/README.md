# {{cookiecutter.project_title}}

## Getting Started

To install, `cd` into the {{cookiecutter.repo_name}} directory (where this README is located)
then run:

    $ pip install .

{{cookiecutter.project_title}} requires Python version 2.7.

If you're not running in a VM or container, it's best to run inside of a
virtual environment (if you have virtualenv installed):

    $ virtualenv venv
    $ source venv/bin/activate
    $ pip install .

After installing, {{cookiecutter.repo_name}} will be available via the `{{cookiecutter.repo_name}}` command.
For more detailed help, run:

    $ {{cookiecutter.repo_name}} --help

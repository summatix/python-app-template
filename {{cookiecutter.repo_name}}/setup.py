"""
setup.py

Setup for the {{cookiecutter.repo_name}} package
"""
import os

from setuptools import find_packages
from setuptools import setup


requirements_txt = os.path.join(
    os.path.dirname(os.path.realpath(__file__)),
    "requirements.txt"
)
requires = []


with open(requirements_txt) as requirements:
    requires = requirements.read().split()


setup(
    name="{{cookiecutter.repo_name}}",
    version="1.0.0",

    packages=find_packages(),

    entry_points={
        "console_scripts": [
            "{{cookiecutter.repo_name}}={{cookiecutter.repo_name}}.app:cli"
        ]
    },
    install_requires=requires
)
